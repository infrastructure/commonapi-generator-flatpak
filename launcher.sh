#!/usr/bin/bash

invoked_as=$(basename "$0")

if [[ "$invoked_as" == "commonapi" ]]; then
  # "/app/bin/commonapi commonapi-core-generator" is a nice shortcut to having
  # to use --command=.
  invoked_as="$1"
  shift

  if [[ ! $invoked_as =~ ^commonapi-(core|dbus|someip)-generator$ ]]; then
    echo 'expected one of' >&2
    find /app/bin -name 'commonapi-*-generator' -printf '- %f\n' | sort
    echo "not: ${invoked_as:-<no argument>}" >&2
    exit 1
  fi
fi

# commonapi-core-generator -> core
name=$(echo "$invoked_as" | cut -d- -f2)

# The generators are built on the Eclipse platform and its OSGI implementation,
# which essentially split a single application into "bundles" / "plug-ins" that
# each provide some piece of functionality. For performance reasons, Eclipse's
# OSGI bundle registry caches information about the loaded bundles. However...
# this cache is only invalidated when the mtime of the bundles is changed, which
# is a problem for Flatpak because all the mtimes are zeroed out (due to OSTree
# details). As a result, when the Flatpak is updated, the parts of cache are
# reused, but the bundle files are re-scanned and assigned new bundle IDs.
# Eclipse tries to load the primary bundle's main class using the old bundle ID
# in the cache, resulting in a NullPointerException when a non-existent bundle
# is used.
#
# As a workaround, we can "abuse" Eclipse's .eclipseproduct files, as the ID and
# version set there are used to determine where the bundle cache is stored. The
# current commit is used as the version, and then when the Flatpak is updated,
# a new cache directory is used.

commit=$(grep '^app-commit=' /.flatpak-info | cut -d= -f2)
[[ -n "$commit" ]] || echo 'WARNING: failed to find commit' >&2

mkdir -p /var/cache/products/
cat > /var/cache/products/$name.properties <<EOF
id=commonapi-$name-generator
version=$commit
EOF

exec /app/tools/$name/commonapi-$name-generator "$@"
