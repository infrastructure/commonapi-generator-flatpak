# CommonAPI Generators Flatpak

This is a Flatpak packaged version of the three primary CommonAPI C++ code
generators:

- commonapi-core-generator
- commonapi-dbus-generator
- commonapi-someip-generator

## Installation

Make sure both Flathub (required because this uses Flathub's OpenJDK SDK) and
the Apertis Flatpak repository are added:

```
$ flatpak remote-add --no-gpg-verify --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
$ flatpak remote-add --if-not-exists apertis https://images.apertis.org/flatpak/repo/apertis.flatpakrepo
```

Then, install the CommonAPI Flatpak:

```
$ flatpak install apertis org.genivi.commonapi
```

## Usage

The single Flatpak contains all three generators, which can be run via the
standard Flatpak syntax for running commands:

```
$ flatpak run --command=commonapi-core-generator org.genivi.commonapi
$ flatpak run --command=commonapi-dbus-generator org.genivi.commonapi
$ flatpak run --command=commonapi-someip-generator org.genivi.commonapi
```

As a shorthand, this Flatpak also has a default command set that lets you pass
the binary name as the first argument to the Flatpak. In other words, you can
also use the following to run the generators:

```
$ flatpak run org.genivi.commonapi commonapi-core-generator
$ flatpak run org.genivi.commonapi commonapi-dbus-generator
$ flatpak run org.genivi.commonapi commonapi-someip-generator
```

## Notes

The Flatpaks do *not* contain the [core runtime
libraries](https://github.com/COVESA/capicxx-core-runtime). These must be
compiled on the host system using host libraries and thus cannot be distributed
inside of a containerized environment.
